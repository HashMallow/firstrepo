import elastic, torob, math
dic = {"%DA%AF%D9%88%D8%B4%DB%8C-%D8%A7%D9%BE%D9%84-%D8%A7%DB%8C%D9%81%D9%88%D9%86-apple":"184", "%DA%AF%D9%88%D8%B4%DB%8C-%D8%B3%D8%A7%D9%85%D8%B3%D9%88%D9%86%DA%AF-samsung":"186","%DA%AF%D9%88%D8%B4%DB%8C-%D8%A7%DA%86-%D8%AA%DB%8C-%D8%B3%DB%8C-htc":"183","%DA%AF%D9%88%D8%B4%DB%8C-%D9%87%D9%88%D8%A7%D9%88%DB%8C-huawei":"185","%DA%AF%D9%88%D8%B4%DB%8C-%D8%A7%D9%84-%D8%AC%DB%8C-lg":"191"}
#dic = {"%DA%AF%D9%88%D8%B4%DB%8C-%D8%A7%D9%BE%D9%84-%D8%A7%DB%8C%D9%81%D9%88%D9%86-apple":"184"}

def pointc1(a, b):
    cnt = 0
    mos = 0
    for it in a:
        if it in b:
            cnt+=1
            mos+=(a.index(it)-b.index(it))**2
    mos = math.sqrt(mos)
    return mos, len(a)

def pointc2(a, b):
    cnt = 0
    mos = 0
    for it in a:
        if it in b:
            cnt+=1
            mos+=(a.index(it)-b.index(it))**2
    mos = math.sqrt(mos)
    return mos, len(b)

def test():
    for it in dic:
        a = elastic.fetch(it)
        b = torob.fetch(it,dic[it])
        print(pointc1(a,b))

test()

