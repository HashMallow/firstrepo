from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.support.ui import WebDriverWait

def fetch(kw):
    button = "//*[@id=\"kibana-body\"]/div/div/div/div[3]/discover-app/div/div[2]/div[2]/div/div[2]/div/doc-table/div/table/tbody/tr[1]/td[1]/i"
    title = "//*[@id=\"kibana-body\"]/div/div/div/div[3]/discover-app/div/div[2]/div[2]/div/div[2]/div/doc-table/div/table/tbody/tr[2]/td/doc-viewer/div/div/render-directive/table/tbody/tr[9]/td[3]/div/span"
    earr=[]

    binary = FirefoxBinary("C:\\Program Files (x86)\\Mozilla Firefox\\Firefox.exe")
    fp = webdriver.FirefoxProfile()
    driver = webdriver.Firefox(firefox_binary=binary, firefox_profile=fp)
    driver.get("http://188.166.16.64:5601/app/kibana#/discover?_g=()&_a=(columns:!(_source),index:torob_mobile,interval:auto,query:(query_string:(analyze_wildcard:!t,query:"+kw+")),sort:!(_score,desc))")
    try:
        element = WebDriverWait(driver, 25).until(
            lambda driver: driver.find_element_by_xpath(button)
        )
    finally:
        for i in range(1,96,2):
            button = "//*[@id=\"kibana-body\"]/div/div/div/div[3]/discover-app/div/div[2]/div[2]/div/div[2]/div/doc-table/div/table/tbody/tr["+str(i)+"]/td[1]"
            title = "//*[@id=\"kibana-body\"]/div/div/div/div[3]/discover-app/div/div[2]/div[2]/div/div[2]/div/doc-table/div/table/tbody/tr["+str(i+1)+"]/td/doc-viewer/div/div/render-directive/table/tbody/tr[9]/td[3]/div/span"
            driver.find_element_by_xpath(button).click()
            t = driver.find_element_by_xpath(title)
            #print(t.text)
            earr.append(t.text)
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        driver.close()
        return earr