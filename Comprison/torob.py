from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

att = ["price_desc","price_newest","price_popularity","price_asc"]
def fetch(kw, cat):
    tarr=[]

    binary = FirefoxBinary("C:\\Program Files (x86)\\Mozilla Firefox\\Firefox.exe")
    fp = webdriver.FirefoxProfile()
    driver = webdriver.Firefox(firefox_binary=binary, firefox_profile=fp)

    for i in range (1,4):
        driver.get("https://torob.com/browse/"+cat+"/"+kw+"/?page="+str(i))
        for j in range(1,25):
            temp = "/html/body/div[2]/div/div[2]/div[2]/div["+str(j)+"]/a/div[2]/h5"
            t = driver.find_element_by_xpath(temp)
            tarr.append(t.text)
    driver.close()
    return tarr