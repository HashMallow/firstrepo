from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

def build():
    dic = {}
    binary = FirefoxBinary("C:\\Program Files (x86)\\Mozilla Firefox\\Firefox.exe")
    fp = webdriver.FirefoxProfile()
    driver = webdriver.Firefox(firefox_binary=binary, firefox_profile=fp)
    driver.get("https://www.amazon.com/")
    for i in range(1,53):
        temp = "//*[@id=\"searchDropdownBox\"]/option["+str(i)+"]"
        t = driver.find_element_by_xpath(temp)
        print(t.text.lstrip())
        print(t.get_attribute("value"))
        dic[t.text.lstrip()]=t.get_attribute("value")
    driver.close()
    return dic

print(build())